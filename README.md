[![Translate - with Weblate](https://img.shields.io/badge/translate%20with-Weblate-green.svg?style=flat)](https://weblate.tusky.app/) [![OpenCollective](https://opencollective.com/tusky/backers/badge.svg)](https://opencollective.com/tusky/) [![Build Status](https://app.bitrise.io/app/a3e773c3c57a894c/status.svg?token=qLu_Ti4Gp2LWcYT4eo2INQ&branch=master)](https://app.bitrise.io/app/a3e773c3c57a894c#/builds) 
# Tusky Libre

![](/fastlane/metadata/android/en-US/images/icon.png)

Tusky Libre is a beautiful Android client for [Mastodon](https://github.com/tootsuite/mastodon). Mastodon is a GNU social-compatible federated social network. That means not one entity controls the whole network, rather, like e-mail, volunteers and organisations operate their own independent servers, users from which can all interact with each other seamlessly.

## Features

- Material Design
- Most Mastodon APIs implemented
- Multi-Account support
- Dark, light and black themes with the possibility to auto-switch based on the time of day
- Drafts - compose toots and save them for later
- Choose between different emoji styles 
- Optimized for all screen sizes
- Completely open-source - no non-free dependencies like Google services
- No instance blocking

### Support

If you have any bug reports, feature requests or questions please open an issue or send a toot to [nvsr@mstdn.jp](https://mstdn.jp/@nvsr)!

### Development

Tusky Libre was forked on 2019-06-04 from the original Tusky project after the Tusky developers decided to implement
the ability to block certain instances from logging in using the application. The goal of this fork is to maintain the
features of the original project, but without the inclusion of instance blocking.
